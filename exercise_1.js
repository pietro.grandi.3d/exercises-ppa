/**
 * Given an array of string, prints out the number of occurrences where a char is repeated.
 *
 */
exports.default = function countRepeatedChars(strings) {
  if (!Array.isArray(strings)) {
    return 0
  }

  let repetitions = 0
  for (i in strings) {
    const asString = strings[i] || ""
    const asSet = typeof asString[Symbol.iterator] === 'function' ? new Set(asString) : new Set()
    if (asSet.size < asString.length) {
      repetitions += 1
    }
  }
  return repetitions
}
