const telnetlib = require("telnetlib")
const { v4 } = require("uuid")

const PORT = process.env.PORT || 9001
const DEBUG = process.env.DEBUG || false

// An in-memory list of the active clients
let cache = []

const server = telnetlib.createServer({}, (connection) => {
  connection.on("negotiated", () => {
    const nextUUID = v4()
    // side-effects plus some memoization to avoid scope-creeping...
    const cleanup = ((uuid) => () => {
      cache = cache.filter(conn => conn.pietro_uuid !== uuid)
      if (DEBUG) {
        console.log(`${uuid} removed, cache size is now ${cache.length}`)
      }
    })(nextUUID)
    connection.pietro_uuid = nextUUID
    connection.socket.on("close", cleanup)
    cache.push(connection)
    if (DEBUG) {
      console.log(`${nextUUID} added, cache size is now ${cache.length}`)
    }
  });

  connection.on("data", (data) => {
    cache.forEach(cachedConnection =>  {
      if (connection.pietro_uuid !== cachedConnection.pietro_uuid) {
        cachedConnection.write(data)
      }
    })
  });
});

server.listen(PORT);
