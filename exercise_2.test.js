const flatten = require("./exercise_2.js").default

describe("flatten", () => {
  describe("when an empty array is passed", () => {
    test("returns []", () => {
      expect(flatten([])).toEqual([])
    })
  })

  describe("when [[1,2,[3]],4] is passed", () => {
    test("returns [1,2,3,4]", () => {
      expect(flatten([[1,2,[3]],4])).toEqual([1,2,3,4])
    })
  })


  describe("when a 1000 items array nested 1000 levels is passed", () => {
    // Please note that the maximum values are totally arbitrary and
    // should be discussed based on the application's requirements.
    const maxLength = 1000
    const maxDepth = 1000
    const input = []
    const output = []
    for (let l = 0; l < maxLength; l += 1) {
      input[l] = [[]]
      let pointer = input[l][0]
      for (let d = 0; d < maxDepth; d += 1) {
        if (maxDepth - d == 1) {
          pointer.push(d + l)
          output.push(d + l)
          continue
        } else {
          pointer.push([])
          pointer = pointer[0]
        }
      }
    }
    test("returns the flattened array", () => {
      expect(flatten(input)).toEqual(output)
    })
  })

  describe("when any non-array is passed", () => {
    test("fall back to []", () => {
      expect(flatten()).toEqual([])
      expect(flatten(null)).toEqual([])
      expect(flatten(1)).toEqual([])
      expect(flatten({})).toEqual([])
      expect(flatten(new Date())).toEqual([])
    })
  })
})
