
let mockConnection = {
  on: (event, cbk) => { mockListeners[event] = cbk },
  socket: { on: (event, cbk) => { mockListeners[event] = cbk }},
}
let mockListenFn
let mockListeners = {}

let mockNextUUID = 1000

jest.mock("uuid",() => ({
  v4: () => ++mockNextUUID
}))

describe("the chat server", () => {
  beforeEach(() => {
    jest.resetModules()
    mockListenFn = jest.fn()
    jest.mock("telnetlib",() => ({
      createServer: (_, handler) => {
        handler(mockConnection)
        return { listen: mockListenFn }
      }
    }))
  })

  test("listens by default on port 9001", () => {
    require("./exercise_3.js")
    expect(mockListenFn).toHaveBeenCalledTimes(1)
    expect(mockListenFn).toHaveBeenCalledWith(9001)
  })

  test("listens on the specified port ", () => {
    process.env = Object.assign(process.env, { PORT: 9999 })
    require("./exercise_3.js")
    expect(mockListenFn).toHaveBeenCalledTimes(1)
    expect(mockListenFn).toHaveBeenCalledWith("9999")
  })

  describe("when two connections are open", () => {
    test("the data from connection is broadcast to the other one", () => {
      jest.resetModules()
      mockListenFn = jest.fn()

      // mocks for connection A
      const mockListeners_A = {}
      const mockConnection_A = {
        on: (event, cbk) => { mockListeners_A[event] = cbk },
        socket: { on: (event, cbk) => { mockListeners_A[event] = cbk }},
        write: jest.fn()
      }

      // mocks for connection B
      const mockListeners_B = {}
      const mockConnection_B = {
        on: (event, cbk) => { mockListeners_B[event] = cbk },
        socket: { on: (event, cbk) => { mockListeners_B[event] = cbk }},
        write: jest.fn()
      }

      let connectionHandler
      jest.mock("telnetlib",() => ({
        createServer: (_, handler) => {
          connectionHandler = handler
          return { listen: jest.fn() }
        }
      }))
      require("./exercise_3.js")

      // simulate connection A
      connectionHandler(mockConnection_A)
      mockListeners_A.negotiated()

      // simulate connection B
      connectionHandler(mockConnection_B)
      mockListeners_B.negotiated()

      mockListeners_A.data("Jest is pretty cool, innit?")

      expect(mockConnection_B.write).toHaveBeenCalledWith("Jest is pretty cool, innit?")
      expect(mockConnection_A.write).not.toHaveBeenCalled()
    })
  })

  describe("when DEBUG is set to 'true'", () => {
    beforeEach(() => {
      mockListeners = {}
      process.env = Object.assign(process.env, { DEBUG: true })
      require("./exercise_3.js")
    })

    describe("and a new connection is open", () => {
      test("it logs the event with the new cache size", () => {
        console.log = jest.fn()
        mockListeners.negotiated()
        expect(console.log.mock.calls[0][0]).toMatch(/cache size is now 1/)
      })

      test("adds the new UUID to the connection object", () => {
        mockListeners.negotiated()
        expect(mockConnection.pietro_uuid).toEqual(mockNextUUID)
      })
    })

    describe("and the connection is closed", () => {
      test("it logs the event with the new cache size", () => {
        console.log = jest.fn()
        mockListeners.negotiated()
        mockListeners.close()
        expect(console.log.mock.calls[1][0]).toMatch(/cache size is now 0/)
      })
    })
  })
})


