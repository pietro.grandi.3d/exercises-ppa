## Setup

Node `v14.15.0` is required (if you use NVM, you can type `nvm use`).

* `npm install`

There is no linter nor code formatter given the small scope of the exercise. I would normally use ESLint and Prettier for a frontend project.

## Running tests

* `npm run test-1` to run the test suite for exercise 1
* `npm run test-2` to run the test suite for exercise 2
* `npm run test-3` to run the test suite for exercise 3
* `npm run test` will run all of them in sequence

### Testing strategies

* I went for 100% coverage for exercise 1 and 2 because they represent the minimum unit of work and are stateless utilities.
* I tested only the happy path for exercise 3 because is an entire application (although small)

## Exercise 1

### Technical choices

I decided to use the Set primitive in Javascript because it is convenient and relatively fast to match a set of unique elements.
If performance is a concern, I'd perform an investigation using regular expressions, arrays, or string methods.

I also used defensive programming checking for non-array input.


## Exercise 2

### Technical choices

I decided to use recursion because I find it familiar and readable.
This is not an optimal solution in Javascript because there is no tail-recursion optimization, therefore the function might eventually crash due to max-stack or out-of-memory errors.
This however applies to very large amount of data. I wrote a test covering a 1000 items array nested up to 1000 times to ensure that there is a clear SLA to be met, although totally arbitrary.

I also used defensive programming checking for non-array input.

## Exercise 3

To start the server: `PORT=9001 node exercise_3.js`. Default port is 9001 if not defined.

Now you can connect from separate terminals via `telnet localhost 9001`. Please verify that you have Telnet installed on you system.

To test the chat server, type a message in any terminal:

1. the message is forwarded to all the other clients
2. the sender doesn't receive an echo
3. cache is updated upon disconnection (run `DEBUG=true node exercise_3` to verify it in the logs)

### Technical choices

* I used [telnetlib](https://github.com/cadpnq/telnetlib#readme) to get an implementation of the Telnet protocol
* individual connections are tracked via UUIDv4 that guarantees a reasonable uniqueness
* the in-memory cache for connections is a simple JS Array - this doesn't scale with multiple instances where a shared in-memory DB like Redis or a message queue may be used instead
* there is no check for memory overflows due to the cache becoming too large, although a cleanup process is in place - a huge amount of connections will eventually crash the server
