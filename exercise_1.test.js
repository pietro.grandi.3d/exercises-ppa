const countRepeatedChars = require("./exercise_1").default

describe("countRepeatedChars", () => {
  describe("when an empty array is passed", () => {
    test("returns 0", () => {
      expect(countRepeatedChars([])).toEqual(0)
    })
  })

  describe("when an array with strings is passed", () => {
    describe("and there are no repetitions", () => {
      test("returns 0", () => {
        expect(countRepeatedChars(["abc", "defg", "h", "il", "mnopq"])).toEqual(0)
      })
    })

    describe("and there repetitions", () => {
      test("returns the correct amount of repetitions", () => {
        expect(countRepeatedChars(["abc", "ddefg", "hh", "ill", "mnopq", "rsr", "tuvzt"])).toEqual(5)
        expect(countRepeatedChars(["a","b","ccdddee"])).toEqual(1)
        expect(countRepeatedChars(["aa","bbb","ccccc"])).toEqual(3)
        expect(countRepeatedChars(["aa","bbccdd","cc", "d"])).toEqual(3)
      })
      test("doesn't count empty strings", () => {
        expect(countRepeatedChars(["", "ddefg", "", "" ])).toEqual(1)
      })
    })
  })

  describe("when any non-array is passed", () => {
    test("fall back to 0", () => {
      expect(countRepeatedChars()).toEqual(0)
      expect(countRepeatedChars(null)).toEqual(0)
      expect(countRepeatedChars(1)).toEqual(0)
      expect(countRepeatedChars({})).toEqual(0)
      expect(countRepeatedChars(new Date())).toEqual(0)
    })
  })

  describe("when non-strings are passed", () => {
    test("doesn't crash", () => {
      const arrayOfWeirdos = [
        undefined, null,1, NaN, {}, [], new Date()
      ]
      expect(countRepeatedChars.bind({}, arrayOfWeirdos)).not.toThrow()
    })

    test("doesn't count them", () => {
      const arrayOfWeirdos = [
        undefined, null,1, NaN, {}, [], new Date(), "dd"
      ]
      expect(countRepeatedChars(arrayOfWeirdos)).toBe(1)
    })
  })
})
