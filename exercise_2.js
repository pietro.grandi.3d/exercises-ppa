/**
 * Write some code that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers.
 *
 */
exports.default = function flatten(array) {
  if (!Array.isArray(array)) {
    return []
  }

  return loop(array)
}

// note that there is no tail-recursion in JS, with large arrays it will fail
// with RangeError: Maximum call stack size exceeded
const loop = (item) => Array.isArray(item) ? item.flatMap(loop) : item
